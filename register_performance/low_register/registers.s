	.text
	.p2align	5
	.globl	_registers
	.globl	registers
_registers:
registers:
	pushq	%rbp
	pushq	%rsi
	pushq	%rbx
	pushq	%r12
	pushq	%r13
	pushq	%r14
	movq	$0, %r14
	movq	$0, %r13
	movq	%rdi, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%rdi, %r8
	movq	%rdi, %r9
	movq	%rdi, %r10
	movq	%rdi, %r11
	movq	%rdi, %rbp
	movq	%rdi, %rbx
	movq	%rdi, %r12
	movq	%rdi, %rax
	addq	%rcx, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	addq	%r12, %rax
	jmp 	L1
L2:
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
	addq	%rdi, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	addq	%r12, %rax
	cmpq	$1000, %r14
	jne 	L3
	cmpq	$10, %r13
	jnb 	L4
	movq	$0, %r14
	incq	%r13
L4:
L3:
	incq	%r14
L1:
	cmpq	$10001, %r14
	jbe 	L2
	addq	%r14, %r13
	addq	%r13, %rcx
	addq	%rdi, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	addq	%r12, %rax
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbx
	popq	%rsi
	popq	%rbp
	ret 
