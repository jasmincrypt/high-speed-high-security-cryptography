	.text
	.p2align	5
	.globl	_registers
	.globl	registers
_registers:
registers:
	pushq	%rbp
	pushq	%rsi
	pushq	%rbx
	pushq	%r12
	pushq	%r13
	pushq	%r14
	movq	$0, %r14
	movq	$0, %r13
	movq	%rdi, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%rdi, %r8
	movq	%rdi, %r9
	movq	%rdi, %r10
	movq	%rdi, %r11
	movq	%rdi, %rbp
	movq	%rdi, %rbx
	movq	%rdi, %r12
	movq	%rdi, %rax
	addq	%rcx, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	addq	%r12, %rax
	jmp 	L1
L2:
	cmpq	$10000, %r14
	jnb 	L15
	addq	$26, %rcx
	imulq	$33, %rcx, %rcx
	subq	$41, %rcx
	xorq	$21, %rcx
	addq	$401, %rcx
	imulq	$98, %rcx, %rcx
	subq	$206, %rcx
	xorq	$94, %rcx
L15:
	cmpq	$9000, %r14
	jnb 	L14
	addq	$26, %rdx
	imulq	$33, %rdx, %rdx
	subq	$41, %rdx
	xorq	$21, %rdx
	addq	$401, %rdx
	imulq	$98, %rdx, %rdx
	subq	$206, %rdx
	xorq	$94, %rdx
L14:
	cmpq	$8000, %r14
	jnb 	L13
	addq	$26, %rsi
	imulq	$33, %rsi, %rsi
	subq	$41, %rsi
	xorq	$21, %rsi
	addq	$401, %rsi
	imulq	$98, %rsi, %rsi
	subq	$206, %rsi
	xorq	$94, %rsi
L13:
	cmpq	$7000, %r14
	jnb 	L12
	addq	$26, %r8
	imulq	$33, %r8, %r8
	subq	$41, %r8
	xorq	$21, %r8
	addq	$401, %r8
	imulq	$98, %r8, %r8
	subq	$206, %r8
	xorq	$94, %r8
L12:
	cmpq	$6000, %r14
	jnb 	L11
	addq	$26, %r9
	imulq	$33, %r9, %r9
	subq	$41, %r9
	xorq	$21, %r9
	addq	$401, %r9
	imulq	$98, %r9, %r9
	subq	$206, %r9
	xorq	$94, %r9
L11:
	cmpq	$5000, %r14
	jnb 	L10
	addq	$26, %r10
	imulq	$33, %r10, %r10
	subq	$41, %r10
	xorq	$21, %r10
	addq	$401, %r10
	imulq	$98, %r10, %r10
	subq	$206, %r10
	xorq	$94, %r10
L10:
	cmpq	$4000, %r14
	jnb 	L9
	addq	$26, %r11
	imulq	$33, %r11, %r11
	subq	$41, %r11
	xorq	$21, %r11
	addq	$401, %r11
	imulq	$98, %r11, %r11
	subq	$206, %r11
	xorq	$94, %r11
L9:
	cmpq	$3000, %r14
	jnb 	L8
	addq	$26, %rbp
	imulq	$33, %rbp, %rbp
	subq	$41, %rbp
	xorq	$21, %rbp
	addq	$401, %rbp
	imulq	$98, %rbp, %rbp
	subq	$206, %rbp
	xorq	$94, %rbp
L8:
	cmpq	$2000, %r14
	jnb 	L7
	addq	$26, %rbx
	imulq	$33, %rbx, %rbx
	subq	$41, %rbx
	xorq	$21, %rbx
	addq	$401, %rbx
	imulq	$98, %rbx, %rbx
	subq	$206, %rbx
	xorq	$94, %rbx
L7:
	cmpq	$1000, %r14
	jnb 	L6
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
L6:
	cmpq	$100, %r14
	jnb 	L5
	addq	$26, %rax
	imulq	$33, %rax, %rax
	subq	$41, %rax
	xorq	$21, %rax
	addq	$401, %rax
	imulq	$98, %rax, %rax
	subq	$206, %rax
	xorq	$94, %rax
L5:
	cmpq	$10000, %r14
	jne 	L3
	cmpq	$100, %r13
	jnb 	L4
	movq	$0, %r14
	incq	%r13
L4:
L3:
	incq	%r14
L1:
	cmpq	$10001, %r14
	jbe 	L2
	addq	%r14, %r13
	addq	%r13, %rcx
	addq	%rdi, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	addq	%r12, %rax
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbx
	popq	%rsi
	popq	%rbp
	ret 
