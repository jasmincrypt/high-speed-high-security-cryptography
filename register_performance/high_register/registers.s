	.text
	.p2align	5
	.globl	_registers
	.globl	registers
_registers:
registers:
	pushq	%rbp
	pushq	%rsi
	pushq	%rbx
	pushq	%r12
	pushq	%r13
	pushq	%r14
	movq	$0, %r14
	movq	$0, %r13
	movq	%rdi, %rax
	movq	%rdi, %rcx
	movq	%rdi, %rdx
	movq	%rdi, %rsi
	movq	%rdi, %r8
	movq	%rdi, %r9
	movq	%rdi, %r10
	movq	%rdi, %r11
	movq	%rdi, %rbp
	movq	%rdi, %rbx
	movq	%rdi, %r12
	addq	%rax, %rax
	addq	%rax, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %r12
	jmp 	L1
L2:
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
	addq	$26, %r12
	imulq	$33, %r12, %r12
	subq	$41, %r12
	xorq	$21, %r12
	addq	$401, %r12
	imulq	$98, %r12, %r12
	subq	$206, %r12
	xorq	$94, %r12
	addq	%rdi, %r12
	addq	%r12, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %rax
	cmpq	$1000, %r14
	jne 	L3
	cmpq	$10, %r13
	jnb 	L4
	movq	$0, %r14
	incq	%r13
L4:
L3:
	incq	%r14
L1:
	cmpq	$10001, %r14
	jbe 	L2
	addq	%r14, %r13
	addq	%r13, %rax
	addq	%rdi, %r12
	addq	%r12, %rcx
	addq	%rcx, %rdx
	addq	%rdx, %rsi
	addq	%rsi, %r8
	addq	%r8, %r9
	addq	%r9, %r10
	addq	%r10, %r11
	addq	%r11, %rbp
	addq	%rbp, %rbx
	addq	%rbx, %rax
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbx
	popq	%rsi
	popq	%rbp
	ret 
