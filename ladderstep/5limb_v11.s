	.text
	.p2align	5
	.globl	_scalarmult
	.globl	scalarmult
_scalarmult:
scalarmult:
	pushq	%rbp
	pushq	%rbx
	pushq	%r12
	pushq	%r13
	pushq	%r14
	subq	$592, %rsp
	movq	(%rsi), %rax
	movq	%rax, (%rsp)
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)
	movq	16(%rsi), %rax
	movq	%rax, 16(%rsp)
	movq	24(%rsi), %rax
	movq	%rax, 24(%rsp)
	movq	(%rsi), %rcx
	movq	$-8, %rax
	andq	%rax, %rcx
	movq	8(%rsi), %rax
	movq	16(%rsi), %r8
	movq	24(%rsi), %r10
	movq	$9223372036854775807, %r9
	andq	%r9, %r10
	movq	$4611686018427387904, %r9
	orq 	%r9, %r10
	movq	%r10, %r9
	movq	%rcx, (%rsi)
	movq	%rax, 8(%rsi)
	movq	%r8, 16(%rsi)
	movq	%r9, 24(%rsi)
	movq	(%rdx), %r8
	movq	%r8, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r8
	movq	%r8, 40(%rsp)
	movq	%rax, %rcx
	movq	8(%rdx), %r8
	movq	%r8, %rax
	shrq	$51, %rcx
	shlq	$13, %r8
	orq 	%r8, %rcx
	movq	$2251799813685247, %r8
	andq	%r8, %rcx
	movq	%rcx, 48(%rsp)
	movq	%rax, %rcx
	movq	16(%rdx), %r8
	movq	%r8, %rax
	shrq	$38, %rcx
	shlq	$26, %r8
	orq 	%r8, %rcx
	movq	$2251799813685247, %r8
	andq	%r8, %rcx
	movq	%rcx, 56(%rsp)
	movq	24(%rdx), %rcx
	shrq	$25, %rax
	shlq	$39, %rcx
	orq 	%rcx, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rax
	movq	%rax, 64(%rsp)
	movq	24(%rdx), %rax
	shrq	$12, %rax
	movq	%rax, 72(%rsp)
	movq	40(%rsp), %rax
	movq	48(%rsp), %rcx
	movq	56(%rsp), %rdx
	movq	64(%rsp), %r8
	movq	72(%rsp), %r9
	movq	%rax, 272(%rsp)
	movq	%rcx, 280(%rsp)
	movq	%rdx, 288(%rsp)
	movq	%r8, 296(%rsp)
	movq	%r9, 304(%rsp)
	movq	%rax, 432(%rsp)
	movq	%rcx, 440(%rsp)
	movq	%rdx, 448(%rsp)
	movq	%r8, 456(%rsp)
	movq	%r9, 464(%rsp)
	movq	$1, 512(%rsp)
	movq	$0, 520(%rsp)
	movq	$0, 528(%rsp)
	movq	$0, 536(%rsp)
	movq	$0, 544(%rsp)
	movq	$0, 552(%rsp)
	movq	$0, 560(%rsp)
	movq	$0, 568(%rsp)
	movq	$0, 576(%rsp)
	movq	$0, 584(%rsp)
	movq	$1, 472(%rsp)
	movq	$0, 480(%rsp)
	movq	$0, 488(%rsp)
	movq	$0, 496(%rsp)
	movq	$0, 504(%rsp)
	movq	$62, %rcx
	movq	$3, %rdx
	movq	$0, 104(%rsp)
L8:
	movq	(%rsi,%rdx,8), %rax
	movq	%rdx, 80(%rsp)
	movq	%rax, 88(%rsp)
L9:
	movq	88(%rsp), %rax
	shrq	%cl, %rax
	movq	%rcx, 96(%rsp)
	andq	$1, %rax
	movq	104(%rsp), %rcx
	xorq	%rax, %rcx
	movq	%rax, 104(%rsp)
	subq	$1, %rcx
	movq	512(%rsp), %rcx
	movq	432(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 512(%rsp)
	movq	%rdx, 432(%rsp)
	movq	552(%rsp), %rcx
	movq	472(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 552(%rsp)
	movq	%rdx, 472(%rsp)
	movq	520(%rsp), %rcx
	movq	440(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 520(%rsp)
	movq	%rdx, 440(%rsp)
	movq	560(%rsp), %rcx
	movq	480(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 560(%rsp)
	movq	%rdx, 480(%rsp)
	movq	528(%rsp), %rcx
	movq	448(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 528(%rsp)
	movq	%rdx, 448(%rsp)
	movq	568(%rsp), %rcx
	movq	488(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 568(%rsp)
	movq	%rdx, 488(%rsp)
	movq	536(%rsp), %rcx
	movq	456(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 536(%rsp)
	movq	%rdx, 456(%rsp)
	movq	576(%rsp), %rcx
	movq	496(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 576(%rsp)
	movq	%rdx, 496(%rsp)
	movq	544(%rsp), %rcx
	movq	464(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 544(%rsp)
	movq	%rdx, 464(%rsp)
	movq	584(%rsp), %rcx
	movq	504(%rsp), %rdx
	movq	%rcx, %rax
	cmovnbq	%rdx, %rcx
	cmovnbq	%rax, %rdx
	movq	%rcx, 584(%rsp)
	movq	%rdx, 504(%rsp)
	movq	512(%rsp), %r10
	movq	520(%rsp), %r11
	movq	528(%rsp), %rbp
	movq	536(%rsp), %rbx
	movq	544(%rsp), %r12
	movq	%r10, %rax
	movq	%r11, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %r8
	movq	%r12, %r9
	addq	552(%rsp), %r10
	addq	560(%rsp), %r11
	addq	568(%rsp), %rbp
	addq	576(%rsp), %rbx
	addq	584(%rsp), %r12
	movq	$4503599627370458, %r13
	leaq	(%rax,%r13), %rax
	subq	552(%rsp), %rax
	movq	$4503599627370494, %r13
	leaq	(%rcx,%r13), %rcx
	subq	560(%rsp), %rcx
	leaq	(%rdx,%r13), %rdx
	subq	568(%rsp), %rdx
	leaq	(%r8,%r13), %r8
	subq	576(%rsp), %r8
	leaq	(%r9,%r13), %r9
	subq	584(%rsp), %r9
	movq	%r10, 192(%rsp)
	movq	%r11, 200(%rsp)
	movq	%rbp, 208(%rsp)
	movq	%rbx, 216(%rsp)
	movq	%r12, 224(%rsp)
	movq	%rax, 112(%rsp)
	movq	%rcx, 120(%rsp)
	movq	%rdx, 128(%rsp)
	movq	%r8, 136(%rsp)
	movq	%r9, 144(%rsp)
	movq	112(%rsp), %rax
	mulq	112(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	120(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	128(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	136(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	144(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	120(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	128(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	136(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	136(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	136(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	128(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	120(%rsp), %rax
	mulq	120(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	136(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	144(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	120(%rsp), %rax
	shlq	$1, %rax
	mulq	128(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	128(%rsp), %rax
	mulq	128(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	120(%rsp), %rax
	shlq	$1, %rax
	mulq	136(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	192(%rsp), %rax
	mulq	192(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	200(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	208(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	216(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	224(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	200(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	208(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	216(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	216(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	216(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	208(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	200(%rsp), %rax
	mulq	200(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	216(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	224(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	200(%rsp), %rax
	shlq	$1, %rax
	mulq	208(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	208(%rsp), %rax
	mulq	208(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	200(%rsp), %rax
	shlq	$1, %rax
	mulq	216(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 312(%rsp)
	movq	%rdx, 320(%rsp)
	movq	%r8, 328(%rsp)
	movq	%r9, 336(%rsp)
	movq	%r10, 344(%rsp)
	movq	$4503599627370458, %rax
	leaq	(%rcx,%rax), %rax
	subq	352(%rsp), %rax
	movq	$4503599627370494, %rcx
	leaq	(%rdx,%rcx), %rdx
	subq	360(%rsp), %rdx
	leaq	(%r8,%rcx), %r8
	subq	368(%rsp), %r8
	leaq	(%r9,%rcx), %r9
	subq	376(%rsp), %r9
	leaq	(%r10,%rcx), %rcx
	subq	384(%rsp), %rcx
	movq	%rax, 392(%rsp)
	movq	%rdx, 400(%rsp)
	movq	%r8, 408(%rsp)
	movq	%r9, 416(%rsp)
	movq	%rcx, 424(%rsp)
	movq	432(%rsp), %r10
	movq	440(%rsp), %r11
	movq	448(%rsp), %rbp
	movq	456(%rsp), %rbx
	movq	464(%rsp), %r12
	movq	%r10, %rax
	movq	%r11, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %r8
	movq	%r12, %r9
	addq	472(%rsp), %r10
	addq	480(%rsp), %r11
	addq	488(%rsp), %rbp
	addq	496(%rsp), %rbx
	addq	504(%rsp), %r12
	movq	$4503599627370458, %r13
	leaq	(%rax,%r13), %rax
	subq	472(%rsp), %rax
	movq	$4503599627370494, %r13
	leaq	(%rcx,%r13), %rcx
	subq	480(%rsp), %rcx
	leaq	(%rdx,%r13), %rdx
	subq	488(%rsp), %rdx
	leaq	(%r8,%r13), %r8
	subq	496(%rsp), %r8
	leaq	(%r9,%r13), %r9
	subq	504(%rsp), %r9
	movq	%r10, 40(%rsp)
	movq	%r11, 48(%rsp)
	movq	%rbp, 56(%rsp)
	movq	%rbx, 64(%rsp)
	movq	%r12, 72(%rsp)
	movq	%rax, 152(%rsp)
	movq	%rcx, 160(%rsp)
	movq	%rdx, 168(%rsp)
	movq	%r8, 176(%rsp)
	movq	%r9, 184(%rsp)
	imulq	$19, 120(%rsp), %rax
	mulq	72(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	64(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 136(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 144(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	112(%rsp), %rax
	mulq	40(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	72(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	64(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 144(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	112(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	120(%rsp), %rax
	mulq	40(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	72(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	64(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	112(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	120(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	128(%rsp), %rax
	mulq	40(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	72(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	64(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	120(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	128(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	136(%rsp), %rax
	mulq	40(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	72(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	120(%rsp), %rax
	mulq	64(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	128(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	136(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	144(%rsp), %rax
	mulq	40(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 232(%rsp)
	movq	%rdx, 240(%rsp)
	movq	%r8, 248(%rsp)
	movq	%r9, 256(%rsp)
	movq	%r10, 264(%rsp)
	imulq	$19, 200(%rsp), %rax
	mulq	184(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 208(%rsp), %rax
	mulq	176(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 216(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 224(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	192(%rsp), %rax
	mulq	152(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 208(%rsp), %rax
	mulq	184(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 216(%rsp), %rax
	mulq	176(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 224(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	192(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	200(%rsp), %rax
	mulq	152(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 216(%rsp), %rax
	mulq	184(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 224(%rsp), %rax
	mulq	176(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	192(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	200(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	208(%rsp), %rax
	mulq	152(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 224(%rsp), %rax
	mulq	184(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	192(%rsp), %rax
	mulq	176(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	200(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	208(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	216(%rsp), %rax
	mulq	152(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	192(%rsp), %rax
	mulq	184(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	200(%rsp), %rax
	mulq	176(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	208(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	216(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	224(%rsp), %rax
	mulq	152(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, %rax
	movq	%rdx, %r11
	movq	%r8, %rbp
	movq	%r9, %rbx
	movq	%r10, %r12
	addq	232(%rsp), %rax
	addq	240(%rsp), %r11
	addq	248(%rsp), %rbp
	addq	256(%rsp), %rbx
	addq	264(%rsp), %r12
	movq	$4503599627370458, %r13
	leaq	(%rcx,%r13), %rcx
	subq	232(%rsp), %rcx
	movq	$4503599627370494, %r13
	leaq	(%rdx,%r13), %rdx
	subq	240(%rsp), %rdx
	leaq	(%r8,%r13), %r8
	subq	248(%rsp), %r8
	leaq	(%r9,%r13), %r9
	subq	256(%rsp), %r9
	leaq	(%r10,%r13), %r10
	subq	264(%rsp), %r10
	movq	%rax, 432(%rsp)
	movq	%r11, 440(%rsp)
	movq	%rbp, 448(%rsp)
	movq	%rbx, 456(%rsp)
	movq	%r12, 464(%rsp)
	movq	%rcx, 472(%rsp)
	movq	%rdx, 480(%rsp)
	movq	%r8, 488(%rsp)
	movq	%r9, 496(%rsp)
	movq	%r10, 504(%rsp)
	movq	432(%rsp), %rax
	mulq	432(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	432(%rsp), %rax
	shlq	$1, %rax
	mulq	440(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	432(%rsp), %rax
	shlq	$1, %rax
	mulq	448(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	432(%rsp), %rax
	shlq	$1, %rax
	mulq	456(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	432(%rsp), %rax
	shlq	$1, %rax
	mulq	464(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	440(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	464(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	448(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	456(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	456(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	456(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	448(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	464(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	440(%rsp), %rax
	mulq	440(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	456(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	464(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	464(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	464(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	440(%rsp), %rax
	shlq	$1, %rax
	mulq	448(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	448(%rsp), %rax
	mulq	448(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	440(%rsp), %rax
	shlq	$1, %rax
	mulq	456(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 432(%rsp)
	movq	%rdx, 440(%rsp)
	movq	%r8, 448(%rsp)
	movq	%r9, 456(%rsp)
	movq	%r10, 464(%rsp)
	movq	472(%rsp), %rax
	mulq	472(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	472(%rsp), %rax
	shlq	$1, %rax
	mulq	480(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	472(%rsp), %rax
	shlq	$1, %rax
	mulq	488(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	472(%rsp), %rax
	shlq	$1, %rax
	mulq	496(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	472(%rsp), %rax
	shlq	$1, %rax
	mulq	504(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	480(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	504(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	488(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	496(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	496(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	496(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	488(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	504(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	480(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	496(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	504(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	504(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	504(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	480(%rsp), %rax
	shlq	$1, %rax
	mulq	488(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	488(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	480(%rsp), %rax
	shlq	$1, %rax
	mulq	496(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 472(%rsp)
	movq	%rdx, 480(%rsp)
	movq	%r8, 488(%rsp)
	movq	%r9, 496(%rsp)
	movq	%r10, 504(%rsp)
	imulq	$19, 280(%rsp), %rax
	mulq	504(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 288(%rsp), %rax
	mulq	496(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 296(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 304(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	272(%rsp), %rax
	mulq	472(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 288(%rsp), %rax
	mulq	504(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 296(%rsp), %rax
	mulq	496(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 304(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	272(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	280(%rsp), %rax
	mulq	472(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 296(%rsp), %rax
	mulq	504(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 304(%rsp), %rax
	mulq	496(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	272(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	280(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	288(%rsp), %rax
	mulq	472(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 304(%rsp), %rax
	mulq	504(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	272(%rsp), %rax
	mulq	496(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	280(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	288(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	296(%rsp), %rax
	mulq	472(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	272(%rsp), %rax
	mulq	504(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	280(%rsp), %rax
	mulq	496(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	288(%rsp), %rax
	mulq	488(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	296(%rsp), %rax
	mulq	480(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	304(%rsp), %rax
	mulq	472(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 472(%rsp)
	movq	%rdx, 480(%rsp)
	movq	%r8, 488(%rsp)
	movq	%r9, 496(%rsp)
	movq	%r10, 504(%rsp)
	imulq	$19, 360(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 368(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 376(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 384(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	352(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 368(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 376(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 384(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	352(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 376(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 384(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	352(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	360(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	368(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 384(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	376(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	352(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	368(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	376(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	384(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 512(%rsp)
	movq	%rdx, 520(%rsp)
	movq	%r8, 528(%rsp)
	movq	%r9, 536(%rsp)
	movq	%r10, 544(%rsp)
	movq	$996687872, %rcx
	movq	392(%rsp), %rax
	mulq	%rcx
	shrq	$13, %rax
	movq	%rax, %r8
	movq	%rdx, %r9
	movq	400(%rsp), %rax
	mulq	%rcx
	shrq	$13, %rax
	addq	%r9, %rax
	movq	%rax, %r10
	movq	%rdx, %r9
	movq	408(%rsp), %rax
	mulq	%rcx
	shrq	$13, %rax
	addq	%r9, %rax
	movq	%rax, %r11
	movq	%rdx, %r9
	movq	416(%rsp), %rax
	mulq	%rcx
	shrq	$13, %rax
	addq	%r9, %rax
	movq	%rax, %rbp
	movq	%rdx, %r9
	movq	424(%rsp), %rax
	mulq	%rcx
	shrq	$13, %rax
	addq	%r9, %rax
	movq	%rax, %rcx
	imulq	$19, %rdx, %rdx
	addq	%r8, %rdx
	movq	%rdx, %rax
	addq	352(%rsp), %rax
	addq	360(%rsp), %r10
	addq	368(%rsp), %r11
	addq	376(%rsp), %rbp
	addq	384(%rsp), %rcx
	movq	%rax, 552(%rsp)
	movq	%r10, 560(%rsp)
	movq	%r11, 568(%rsp)
	movq	%rbp, 576(%rsp)
	movq	%rcx, 584(%rsp)
	imulq	$19, 400(%rsp), %rax
	mulq	584(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 408(%rsp), %rax
	mulq	576(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 416(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 424(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	392(%rsp), %rax
	mulq	552(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 408(%rsp), %rax
	mulq	584(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 416(%rsp), %rax
	mulq	576(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 424(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	392(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	400(%rsp), %rax
	mulq	552(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 416(%rsp), %rax
	mulq	584(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 424(%rsp), %rax
	mulq	576(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	392(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	400(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	408(%rsp), %rax
	mulq	552(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 424(%rsp), %rax
	mulq	584(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	392(%rsp), %rax
	mulq	576(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	400(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	408(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	416(%rsp), %rax
	mulq	552(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	392(%rsp), %rax
	mulq	584(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	400(%rsp), %rax
	mulq	576(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	408(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	416(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	424(%rsp), %rax
	mulq	552(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 552(%rsp)
	movq	%rdx, 560(%rsp)
	movq	%r8, 568(%rsp)
	movq	%r9, 576(%rsp)
	movq	%r10, 584(%rsp)
	movq	96(%rsp), %rcx
	decq	%rcx
	cmpq	$0, %rcx
	jnl 	L9
	movq	$63, %rcx
	movq	80(%rsp), %rdx
	decq	%rdx
	cmpq	$0, %rdx
	jnl 	L8
	movq	552(%rsp), %rax
	mulq	552(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	552(%rsp), %rax
	shlq	$1, %rax
	mulq	560(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	552(%rsp), %rax
	shlq	$1, %rax
	mulq	568(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	552(%rsp), %rax
	shlq	$1, %rax
	mulq	576(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	552(%rsp), %rax
	shlq	$1, %rax
	mulq	584(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	560(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	584(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	568(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	576(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	576(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	576(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	568(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	584(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	560(%rsp), %rax
	mulq	560(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	576(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	584(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	584(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	584(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	560(%rsp), %rax
	shlq	$1, %rax
	mulq	568(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	568(%rsp), %rax
	mulq	568(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	560(%rsp), %rax
	shlq	$1, %rax
	mulq	576(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 232(%rsp)
	movq	%rdx, 240(%rsp)
	movq	%r8, 248(%rsp)
	movq	%r9, 256(%rsp)
	movq	%r10, 264(%rsp)
	movq	232(%rsp), %rax
	mulq	232(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	232(%rsp), %rax
	shlq	$1, %rax
	mulq	240(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	232(%rsp), %rax
	shlq	$1, %rax
	mulq	248(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	232(%rsp), %rax
	shlq	$1, %rax
	mulq	256(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	232(%rsp), %rax
	shlq	$1, %rax
	mulq	264(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	240(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	264(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	248(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	256(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	256(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	256(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	248(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	264(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	240(%rsp), %rax
	mulq	240(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	256(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	264(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	264(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	264(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	240(%rsp), %rax
	shlq	$1, %rax
	mulq	248(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	248(%rsp), %rax
	mulq	248(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	240(%rsp), %rax
	shlq	$1, %rax
	mulq	256(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	imulq	$19, 560(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 568(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 576(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 584(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	552(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 568(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 576(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 584(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	552(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	560(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 576(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 584(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	552(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	560(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	568(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 584(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	552(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	560(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	568(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	576(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	552(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	560(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	568(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	576(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	584(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 312(%rsp)
	movq	%rdx, 320(%rsp)
	movq	%r8, 328(%rsp)
	movq	%r9, 336(%rsp)
	movq	%r10, 344(%rsp)
	imulq	$19, 240(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 248(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 256(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 264(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	232(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 248(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 256(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 264(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	232(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	240(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 256(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 264(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	232(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	240(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	248(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 264(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	232(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	240(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	248(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	256(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	232(%rsp), %rax
	mulq	344(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	240(%rsp), %rax
	mulq	336(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	248(%rsp), %rax
	mulq	328(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	256(%rsp), %rax
	mulq	320(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	264(%rsp), %rax
	mulq	312(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 272(%rsp)
	movq	%rdx, 280(%rsp)
	movq	%r8, 288(%rsp)
	movq	%r9, 296(%rsp)
	movq	%r10, 304(%rsp)
	movq	272(%rsp), %rax
	mulq	272(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	272(%rsp), %rax
	shlq	$1, %rax
	mulq	280(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	272(%rsp), %rax
	shlq	$1, %rax
	mulq	288(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	272(%rsp), %rax
	shlq	$1, %rax
	mulq	296(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	272(%rsp), %rax
	shlq	$1, %rax
	mulq	304(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	280(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	304(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	288(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	296(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	296(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	296(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	288(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	304(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	280(%rsp), %rax
	mulq	280(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	296(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	304(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	304(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	304(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	280(%rsp), %rax
	shlq	$1, %rax
	mulq	288(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	288(%rsp), %rax
	mulq	288(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	280(%rsp), %rax
	shlq	$1, %rax
	mulq	296(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	imulq	$19, 320(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 328(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 336(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 344(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	312(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 328(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 336(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 344(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	312(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	320(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 336(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 344(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	312(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	320(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	328(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 344(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	312(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	320(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	328(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	336(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	312(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	320(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	328(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	336(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	344(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 392(%rsp)
	movq	%rdx, 400(%rsp)
	movq	%r8, 408(%rsp)
	movq	%r9, 416(%rsp)
	movq	%r10, 424(%rsp)
	movq	392(%rsp), %rax
	mulq	392(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	392(%rsp), %rax
	shlq	$1, %rax
	mulq	400(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	392(%rsp), %rax
	shlq	$1, %rax
	mulq	408(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	392(%rsp), %rax
	shlq	$1, %rax
	mulq	416(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	392(%rsp), %rax
	shlq	$1, %rax
	mulq	424(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	400(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	424(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	408(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	416(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	416(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	416(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	408(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	424(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	400(%rsp), %rax
	mulq	400(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	416(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	424(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	424(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	424(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	400(%rsp), %rax
	shlq	$1, %rax
	mulq	408(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	408(%rsp), %rax
	mulq	408(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	400(%rsp), %rax
	shlq	$1, %rax
	mulq	416(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$3, 80(%rsp)
L7:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L7
	imulq	$19, 400(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 408(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 416(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 424(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	392(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 408(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 416(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 424(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	392(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	400(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 416(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 424(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	392(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	400(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	408(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 424(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	392(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	400(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	408(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	416(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	392(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	400(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	408(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	416(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	424(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 112(%rsp)
	movq	%rdx, 120(%rsp)
	movq	%r8, 128(%rsp)
	movq	%r9, 136(%rsp)
	movq	%r10, 144(%rsp)
	movq	112(%rsp), %rax
	mulq	112(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	120(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	128(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	136(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	112(%rsp), %rax
	shlq	$1, %rax
	mulq	144(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	120(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	128(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	136(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	136(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	136(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	128(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	120(%rsp), %rax
	mulq	120(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	136(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	144(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	144(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	120(%rsp), %rax
	shlq	$1, %rax
	mulq	128(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	128(%rsp), %rax
	mulq	128(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	120(%rsp), %rax
	shlq	$1, %rax
	mulq	136(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$8, 80(%rsp)
L6:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L6
	imulq	$19, 120(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 136(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 144(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	112(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 144(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	112(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	120(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	112(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	120(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	128(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	120(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	128(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	136(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	120(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	128(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	136(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	144(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 192(%rsp)
	movq	%rdx, 200(%rsp)
	movq	%r8, 208(%rsp)
	movq	%r9, 216(%rsp)
	movq	%r10, 224(%rsp)
	movq	192(%rsp), %rax
	mulq	192(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	200(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	208(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	216(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	192(%rsp), %rax
	shlq	$1, %rax
	mulq	224(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	200(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	208(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	216(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	216(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	216(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	208(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	200(%rsp), %rax
	mulq	200(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	216(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	224(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	224(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	200(%rsp), %rax
	shlq	$1, %rax
	mulq	208(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	208(%rsp), %rax
	mulq	208(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	200(%rsp), %rax
	shlq	$1, %rax
	mulq	216(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$18, 80(%rsp)
L5:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L5
	imulq	$19, 200(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 208(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 216(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 224(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	192(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 208(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 216(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 224(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	192(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	200(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 216(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 224(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	192(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	200(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	208(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 224(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	192(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	200(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	208(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	216(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	192(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	200(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	208(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	216(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	224(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$8, 80(%rsp)
L4:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L4
	imulq	$19, 120(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 136(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 144(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	112(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 128(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 144(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	112(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	120(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 136(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	112(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	120(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	128(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 144(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	120(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	128(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	136(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	112(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	120(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	128(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	136(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	144(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%r8, 56(%rsp)
	movq	%r9, 64(%rsp)
	movq	%r10, 72(%rsp)
	movq	40(%rsp), %rax
	mulq	40(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	40(%rsp), %rax
	shlq	$1, %rax
	mulq	48(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	40(%rsp), %rax
	shlq	$1, %rax
	mulq	56(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	40(%rsp), %rax
	shlq	$1, %rax
	mulq	64(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	40(%rsp), %rax
	shlq	$1, %rax
	mulq	72(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	48(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	72(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	56(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	64(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	64(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	64(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	56(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	72(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	48(%rsp), %rax
	mulq	48(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	64(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	72(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	72(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	72(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	48(%rsp), %rax
	shlq	$1, %rax
	mulq	56(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	56(%rsp), %rax
	mulq	56(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	48(%rsp), %rax
	shlq	$1, %rax
	mulq	64(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$48, 80(%rsp)
L3:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L3
	imulq	$19, 48(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 56(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 64(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 72(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	40(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 56(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 64(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 72(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	40(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	48(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 64(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 72(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	40(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	48(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	56(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 72(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	40(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	48(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	56(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	64(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	40(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	48(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	56(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	64(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	72(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 152(%rsp)
	movq	%rdx, 160(%rsp)
	movq	%r8, 168(%rsp)
	movq	%r9, 176(%rsp)
	movq	%r10, 184(%rsp)
	movq	152(%rsp), %rax
	mulq	152(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	152(%rsp), %rax
	shlq	$1, %rax
	mulq	160(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	152(%rsp), %rax
	shlq	$1, %rax
	mulq	168(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	152(%rsp), %rax
	shlq	$1, %rax
	mulq	176(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	152(%rsp), %rax
	shlq	$1, %rax
	mulq	184(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	160(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	184(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	168(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	176(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	176(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	176(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	168(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	184(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	160(%rsp), %rax
	mulq	160(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	176(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	184(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	184(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	184(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	160(%rsp), %rax
	shlq	$1, %rax
	mulq	168(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	168(%rsp), %rax
	mulq	168(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	160(%rsp), %rax
	shlq	$1, %rax
	mulq	176(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$98, 80(%rsp)
L2:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L2
	imulq	$19, 160(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 168(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 176(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 184(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	152(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 168(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 176(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 184(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	152(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	160(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 176(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 184(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	152(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	160(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	168(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 184(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	152(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	160(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	168(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	176(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	152(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	160(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	168(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	176(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	184(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	$48, 80(%rsp)
L1:
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	80(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 80(%rsp)
	jnb 	L1
	imulq	$19, 48(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 56(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 64(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 72(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	40(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 56(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 64(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 72(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	40(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	48(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 64(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 72(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	40(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	48(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	56(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 72(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	40(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	48(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	56(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	64(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	40(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	48(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	56(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	64(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	72(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	movq	352(%rsp), %rax
	mulq	352(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	360(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	352(%rsp), %rax
	shlq	$1, %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	360(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	376(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	368(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	360(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	376(%rsp), %rax
	imulq	$38, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	384(%rsp), %rax
	imulq	$19, %rax, %rax
	mulq	384(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	368(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	360(%rsp), %rax
	shlq	$1, %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 352(%rsp)
	movq	%rdx, 360(%rsp)
	movq	%r8, 368(%rsp)
	movq	%r9, 376(%rsp)
	movq	%r10, 384(%rsp)
	imulq	$19, 280(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 288(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 296(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 304(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	272(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 288(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 296(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 304(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	272(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	280(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 296(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 304(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	272(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	280(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	288(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 304(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	272(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	280(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	288(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	296(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	272(%rsp), %rax
	mulq	384(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	280(%rsp), %rax
	mulq	376(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	288(%rsp), %rax
	mulq	368(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	296(%rsp), %rax
	mulq	360(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	304(%rsp), %rax
	mulq	352(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, 552(%rsp)
	movq	%rdx, 560(%rsp)
	movq	%r8, 568(%rsp)
	movq	%r9, 576(%rsp)
	movq	%r10, 584(%rsp)
	imulq	$19, 560(%rsp), %rax
	mulq	544(%rsp)
	movq	%rax, %rcx
	movq	%rdx, %r8
	imulq	$19, 568(%rsp), %rax
	mulq	536(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 576(%rsp), %rax
	mulq	528(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 584(%rsp), %rax
	mulq	520(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	movq	552(%rsp), %rax
	mulq	512(%rsp)
	addq	%rax, %rcx
	adcq	%rdx, %r8
	imulq	$19, 568(%rsp), %rax
	mulq	544(%rsp)
	movq	%rax, %r9
	movq	%rdx, %r10
	imulq	$19, 576(%rsp), %rax
	mulq	536(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 584(%rsp), %rax
	mulq	528(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	552(%rsp), %rax
	mulq	520(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	movq	560(%rsp), %rax
	mulq	512(%rsp)
	addq	%rax, %r9
	adcq	%rdx, %r10
	imulq	$19, 576(%rsp), %rax
	mulq	544(%rsp)
	movq	%rax, %r11
	movq	%rdx, %rbp
	imulq	$19, 584(%rsp), %rax
	mulq	536(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	552(%rsp), %rax
	mulq	528(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	560(%rsp), %rax
	mulq	520(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	movq	568(%rsp), %rax
	mulq	512(%rsp)
	addq	%rax, %r11
	adcq	%rdx, %rbp
	imulq	$19, 584(%rsp), %rax
	mulq	544(%rsp)
	movq	%rax, %rbx
	movq	%rdx, %r12
	movq	552(%rsp), %rax
	mulq	536(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	560(%rsp), %rax
	mulq	528(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	568(%rsp), %rax
	mulq	520(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	576(%rsp), %rax
	mulq	512(%rsp)
	addq	%rax, %rbx
	adcq	%rdx, %r12
	movq	552(%rsp), %rax
	mulq	544(%rsp)
	movq	%rax, %r13
	movq	%rdx, %r14
	movq	560(%rsp), %rax
	mulq	536(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	568(%rsp), %rax
	mulq	528(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	576(%rsp), %rax
	mulq	520(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	584(%rsp), %rax
	mulq	512(%rsp)
	addq	%rax, %r13
	adcq	%rdx, %r14
	movq	$2251799813685247, %rax
	shldq	$13, %rcx, %r8
	andq	%rax, %rcx
	shldq	$13, %r9, %r10
	andq	%rax, %r9
	leaq	(%r9,%r8), %rdx
	shldq	$13, %r11, %rbp
	andq	%rax, %r11
	leaq	(%r11,%r10), %r8
	shldq	$13, %rbx, %r12
	andq	%rax, %rbx
	leaq	(%rbx,%rbp), %r9
	shldq	$13, %r13, %r14
	andq	%rax, %r13
	leaq	(%r13,%r12), %r10
	imulq	$19, %r14, %r14
	leaq	(%rcx,%r14), %r11
	movq	%r11, %rcx
	shrq	$51, %rcx
	leaq	(%rdx,%rcx), %rdx
	andq	%rax, %r11
	movq	%rdx, %rcx
	shrq	$51, %rcx
	leaq	(%r8,%rcx), %r8
	andq	%rax, %rdx
	movq	%r8, %rcx
	shrq	$51, %rcx
	leaq	(%r9,%rcx), %r9
	andq	%rax, %r8
	movq	%r9, %rcx
	shrq	$51, %rcx
	leaq	(%r10,%rcx), %r10
	andq	%rax, %r9
	movq	%r10, %rcx
	shrq	$51, %rcx
	imulq	$19, %rcx, %rcx
	leaq	(%r11,%rcx), %rcx
	andq	%rax, %r10
	movq	%rcx, %rax
	movq	%rax, %r11
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r11
	leaq	(%rax,%rdx), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	leaq	(%rax,%r8), %rax
	movq	%rax, %r8
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r8
	leaq	(%rax,%r9), %rax
	movq	%rax, %r9
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r9
	leaq	(%rax,%r10), %rax
	movq	%rax, %r10
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r10
	imulq	$19, %rax, %rax
	leaq	(%rax,%r11), %rax
	movq	%rax, %r11
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r11
	leaq	(%rax,%rdx), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	leaq	(%rax,%r8), %rax
	movq	%rax, %r8
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r8
	leaq	(%rax,%r9), %rax
	movq	%rax, %r9
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r9
	leaq	(%rax,%r10), %rax
	movq	%rax, %r10
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r10
	imulq	$19, %rax, %rax
	leaq	(%rax,%r11), %rax
	movq	%rax, %r11
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %r11
	leaq	(%rax,%rdx), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	movq	%rdx, %rbp
	leaq	(%rax,%r8), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	movq	%rdx, %rbx
	leaq	(%rax,%r9), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	movq	%rdx, %r12
	leaq	(%rax,%r10), %rax
	movq	%rax, %rdx
	shrq	$51, %rax
	movq	$2251799813685247, %rcx
	andq	%rcx, %rdx
	movq	%rdx, %r13
	imulq	$19, %rax, %rax
	leaq	(%rax,%r11), %rax
	movq	%rax, %r10
	movq	$1, %rdx
	movq	$0, %rax
	movq	%r10, %rcx
	movq	$2251799813685229, %r9
	cmpq	%r9, %rcx
	cmovbq	%rax, %rdx
	movq	$2251799813685247, %r8
	movq	%rbp, %rcx
	cmpq	%r8, %rcx
	cmovneq	%rax, %rdx
	movq	%rbx, %rcx
	cmpq	%r8, %rcx
	cmovneq	%rax, %rdx
	movq	%r12, %rcx
	cmpq	%r8, %rcx
	cmovneq	%rax, %rdx
	movq	%r13, %rcx
	cmpq	%r8, %rcx
	cmovneq	%rax, %rdx
	negq	%rdx
	andq	%rdx, %r8
	andq	%rdx, %r9
	subq	%r9, %r10
	subq	%r8, %rbp
	subq	%r8, %rbx
	subq	%r8, %r12
	subq	%r8, %r13
	movq	%r10, %rcx
	movq	%rbp, %rax
	shlq	$51, %rax
	orq 	%rax, %rcx
	movq	%rcx, (%rdi)
	movq	%rbp, %rax
	movq	%rbx, %rcx
	shrq	$13, %rax
	shlq	$38, %rcx
	orq 	%rcx, %rax
	movq	%rax, 8(%rdi)
	movq	%rbx, %rax
	movq	%r12, %rcx
	shrq	$26, %rax
	shlq	$25, %rcx
	orq 	%rcx, %rax
	movq	%rax, 16(%rdi)
	movq	%r12, %rax
	movq	%r13, %rcx
	shrq	$39, %rax
	shlq	$12, %rcx
	orq 	%rcx, %rax
	movq	%rax, 24(%rdi)
	movq	(%rsp), %rax
	movq	%rax, (%rsi)
	movq	8(%rsp), %rax
	movq	%rax, 8(%rsi)
	movq	16(%rsp), %rax
	movq	%rax, 16(%rsi)
	movq	24(%rsp), %rax
	movq	%rax, 24(%rsi)
	addq	$592, %rsp
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbx
	popq	%rbp
	ret 
