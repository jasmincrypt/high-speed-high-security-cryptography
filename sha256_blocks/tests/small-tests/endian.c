#include <stdio.h>
#include <stdint.h>

int
main (int argc, char *argv[])
{
  uint8_t x[8] = {0xaa, 0xbb, 0xcc, 0xdd, 0x00, 0x00, 0x00, 0x00};
  uint32_t x32_a = *( (uint32_t *) x);
  uint64_t x64_a = *( (uint64_t *) x);
  x32_a = __builtin_bswap32(x32_a);
  x64_a = __builtin_bswap64(x64_a);
  printf("%x\n",x32_a);
  printf("%lx\n",x64_a);
  return 0;
}
