	.text
	.p2align	5
	.globl	_test
	.globl	test
_test:
test:
	pushq	%rbp
	pushq	%rdi
	pushq	%rsi
	subq	$40, %rsp
	movq	%rdi, %rax
	movq	%rax, %rcx
	movq	%rax, %rdx
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	%rax, %r8
	movq	%rax, (%rsp)
	movq	%rax, 8(%rsp)
	movq	%rax, 16(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rax, 32(%rsp)
	addq	(%rsp), %rcx
	addq	8(%rsp), %rdx
	addq	16(%rsp), %rsi
	addq	24(%rsp), %rdi
	addq	32(%rsp), %r8
	addq	%rcx, %rax
	addq	%rdx, %rax
	addq	%rsi, %rax
	addq	%rdi, %rax
	addq	%r8, %rax
	addq	$40, %rsp
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret 
