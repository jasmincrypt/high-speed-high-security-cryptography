	.text
	.p2align	5
	.globl	_scalarmult
	.globl	scalarmult
_scalarmult:
scalarmult:
	pushq	%rbp
	pushq	%rdi
	pushq	%rsi
	movq	%rdi, %rax
	movq	$62, %rcx
	movq	$3, %r8
L1:
	leaq	(%rax,%r8,8), %rdx
L2:
	movq	%rdx, %rsi
	shrq	%cl, %rsi
	movq	%rdx, %rax
	incq	%rax
	addq	$2, %rax
	addq	$3, %rax
	addq	$4, %rax
	addq	$2, %rsi
	movq	$3, %rdi
	addq	%rdi, %rsi
	decq	%rcx
	cmpq	$0, %rcx
	jnl 	L2
	movq	$63, %rcx
	decq	%r8
	addq	%rsi, %rax
	cmpq	$0, %r8
	jnl 	L1
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret 
