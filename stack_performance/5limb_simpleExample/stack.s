	.text
	.p2align	5
	.globl	_scalarmult
	.globl	scalarmult
_scalarmult:
scalarmult:
	pushq	%rbp
	pushq	%rdi
	pushq	%rsi
	subq	$16, %rsp
	movq	%rdi, %rax
	movq	$62, %rcx
	movq	$3, %rdi
L1:
	leaq	(%rax,%rdi,8), %rdx
	movq	%rdi, (%rsp)
L2:
	movq	%rdx, %rsi
	shrq	%cl, %rsi
	movq	%rcx, 8(%rsp)
	movq	%rdx, %rax
	incq	%rax
	addq	$2, %rax
	addq	$3, %rax
	addq	$4, %rax
	addq	$2, %rsi
	movq	$3, %rcx
	addq	%rcx, %rsi
	movq	8(%rsp), %rcx
	decq	%rcx
	cmpq	$0, %rcx
	jnl 	L2
	movq	$63, %rcx
	movq	(%rsp), %rdi
	decq	%rdi
	addq	%rsi, %rax
	cmpq	$0, %rdi
	jnl 	L1
	addq	$16, %rsp
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret 
