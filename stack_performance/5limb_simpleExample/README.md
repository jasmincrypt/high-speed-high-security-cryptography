# Results - i5-4210U CPU @ 1.70GHz
```
echo "1" | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo

./stack
run,percentil25,percentil75,median
0,2520,2526,2526
1,2520,2526,2526

./nostack
run,percentil25,percentil75,median
0,1596,1602,1602
1,1596,1602,1602
2,1596,1602,1602

```
