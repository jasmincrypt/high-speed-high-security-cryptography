# Results - i5-4210U CPU @ 1.70GHz
```
echo "1" | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo

./registers_nostack
2356481689589249830
run,percentil25,percentil75,median
0,8952,8958,8958
1,8952,8958,8958
2,8952,8958,8958

./registers_stack
2356481689589249830
run,percentil25,percentil75,median
0,8958,8964,8964
1,8958,8964,8964
2,8958,8964,8964
```
