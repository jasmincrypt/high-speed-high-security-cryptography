	.file	"test_blocksC.c"
	.section	.rodata
	.align 8
.LC0:
	.string	"--------------------------------------"
.LC1:
	.string	"%x - "
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	addq	$-128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$1779033703, -112(%rbp)
	movl	$-1150833019, -108(%rbp)
	movl	$1013904242, -104(%rbp)
	movl	$-1521486534, -100(%rbp)
	movl	$1359893119, -96(%rbp)
	movl	$-1694144372, -92(%rbp)
	movl	$528734635, -88(%rbp)
	movl	$1541459225, -84(%rbp)
	movl	$0, -116(%rbp)
	jmp	.L2
.L3:
	movl	-116(%rbp), %eax
	cltq
	movzbl	tmpin.2317(%rax), %eax
	movl	%eax, %edx
	movl	-116(%rbp), %eax
	cltq
	movb	%dl, -80(%rbp,%rax)
	addl	$1, -116(%rbp)
.L2:
	cmpl	$63, -116(%rbp)
	jle	.L3
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	sha256_compress
	movl	$10, %edi
	call	putchar
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$10, %edi
	call	putchar
	movl	$0, -116(%rbp)
	jmp	.L4
.L5:
	movl	-116(%rbp), %eax
	cltq
	movl	-112(%rbp,%rax,4), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -116(%rbp)
.L4:
	cmpl	$7, -116(%rbp)
	jle	.L5
	movl	$10, %edi
	call	putchar
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L7
	call	__stack_chk_fail
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 32
	.type	tmpin.2317, @object
	.size	tmpin.2317, 64
tmpin.2317:
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
