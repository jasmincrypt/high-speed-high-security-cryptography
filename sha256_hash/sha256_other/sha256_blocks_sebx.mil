//SCHED = i & 0xf; MOD::0-f
fn SCHED(inline int i, reg u64 x) -> reg u64
{
  x = i & 0xf;
  x *= 8;

  return x;
}

fn SCHEDM(inline int i, inline int m, reg u64 x) -> reg u64
{
  x = i - m;
  x &= 0xf;
  x *= 8;

  return x;
}

fn roundtail(reg u64[8] tl, inline int k, stack u64 ebx) -> reg u64[8]
{
  reg u64 ecx;
  reg u64 edx;
  reg u64 eax;
  inline int tmp; 

  /* Part 0 */
  ecx = tl[4];
  edx = tl[4];
  eax = tl[4];
  
  tmp = 11;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 25;
  edx = #x86_ROR32(edx, tmp);
  tmp = 6;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  tl[7] = #x86_ADD32(tl[7], ebx);
  ecx = tl[6];
  ecx ^= tl[5];
  ecx &= tl[4];
  ecx ^= tl[6];

  eax = k + ecx + eax;
  tl[7] = #x86_ADD32(tl[7], eax);

  /* Part 1 */
  tl[3] = #x86_ADD32(tl[3], tl[7]);

  /* Part 2 */
  ecx = tl[0];
  edx = tl[0];
  eax = tl[0];

  tmp = 13;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 22;
  edx = #x86_ROR32(edx, tmp);
  tmp = 2;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  ecx = tl[2];
  tl[7] = #x86_ADD32(tl[7], eax);

  eax = tl[2];
  eax |= tl[1];
  ecx &= tl[1];
  eax &= tl[0];
  eax |= ecx;
  tl[7] = #x86_ADD32(tl[7], eax);

  return tl;
}

fn roundtailX(reg u64[8] tl, inline int k, stack u64 ebx) -> reg u64[8]
{
  reg u64 ecx;
  reg u64 edx;
  reg u64 eax;
  inline int tmp; 

  /* Part 0 */
  ecx = tl[4];
  edx = tl[4];
  eax = tl[4];
  
  tmp = 11;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 25;
  edx = #x86_ROR32(edx, tmp);
  tmp = 6;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  tl[7] = #x86_ADD32(tl[7], ebx);
  ecx = tl[6];
  ecx ^= tl[5];
  ecx &= tl[4];
  ecx ^= tl[6];

  eax = -k + ecx + eax;
  tl[7] = #x86_ADD32(tl[7], eax);

  /* Part 1 */
  tl[3] = #x86_ADD32(tl[3], tl[7]);

  /* Part 2 */
  ecx = tl[0];
  edx = tl[0];
  eax = tl[0];

  tmp = 13;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 22;
  edx = #x86_ROR32(edx, tmp);
  tmp = 2;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  ecx = tl[2];
  tl[7] = #x86_ADD32(tl[7], eax);

  eax = tl[2];
  eax |= tl[1];
  ecx &= tl[1];
  eax &= tl[0];
  eax |= ecx;
  tl[7] = #x86_ADD32(tl[7], eax);

  return tl;
}


fn ROUNDa(inline int i, reg u64[8] tl, reg u64 rsi, inline int k, reg u64 rsp) -> reg u64[8]
{
  reg u64 ebx;
  stack u64 sebx;
  reg u64 x;
  
  ebx = #x86_MOV32([rsi + i*4]);
  ebx = #x86_BSWAP32(ebx);

  x = SCHED(i, x);
  [rsp + x] = ebx;
  sebx = ebx;

  tl = roundtail(tl, k, sebx);
  return tl;
}

fn ROUNDaX(inline int i, reg u64[8] tl, reg u64 rsi, inline int k, reg u64 rsp) -> reg u64[8]
{
  reg u64 ebx;
  stack u64 sebx;
  reg u64 x;
  
  ebx = #x86_MOV32([rsi + i*4]);
  ebx = #x86_BSWAP32(ebx);

  x = SCHED(i, x);
  [rsp + x] = ebx;
  sebx = ebx;

  tl = roundtailX(tl, k, sebx);
  return tl;
}
fn ROUNDb(inline int i, reg u64[8] tl, inline int k, reg u64 rsp) -> reg u64[8]
{
  reg u64 eax;
  reg u64 ebx;
  stack u64 sebx;
  reg u64 ecx;
  reg u64 edx;
  reg u64 x;//SCHED
  inline int tmp;

  tmp = 15;
  x = SCHEDM(i, tmp, x);
  eax = [rsp  + x];
  tmp = 16;
  x = SCHEDM(i, tmp, x);
  ebx = [rsp + x];
  tmp = 7;
  x = SCHEDM(i, tmp, x);
  ecx = [rsp + x];
  ebx = #x86_ADD32(ebx, ecx);

  ecx = eax;
  edx = eax;

  tmp = 18;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 3;
  edx = #x86_SHR32(edx, tmp);
  tmp = 7;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);
  
  tmp = 2;
  x = SCHEDM(i, tmp, x);
  eax = [rsp + x];

  ecx = eax;
  edx = eax;
  
  tmp = 19;
  ecx = #x86_ROR32(ecx, tmp); 
  tmp = 10;
  edx = #x86_SHR32(edx, tmp);
  tmp = 17;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);

  x = SCHED(i, x);
  [rsp + x] = ebx;

  sebx = ebx;
  tl = roundtail(tl, k, sebx);

  return tl;
}

fn ROUNDbX(inline int i, reg u64[8] tl, inline int k, reg u64 rsp) -> reg u64[8]
{
  reg u64 eax;
  reg u64 ebx;
  stack u64 sebx;
  reg u64 ecx;
  reg u64 edx;
  reg u64 x;//SCHED
  inline int tmp;

  tmp = 15;
  x = SCHEDM(i, tmp, x);
  eax = [rsp  + x];
  tmp = 16;
  x = SCHEDM(i, tmp, x);
  ebx = [rsp + x];
  tmp = 7;
  x = SCHEDM(i, tmp, x);
  ecx = [rsp + x];
  ebx = #x86_ADD32(ebx, ecx);

  ecx = eax;
  edx = eax;

  tmp = 18;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 3;
  edx = #x86_SHR32(edx, tmp);
  tmp = 7;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);
  
  tmp = 2;
  x = SCHEDM(i, tmp, x);
  eax = [rsp + x];

  ecx = eax;
  edx = eax;
  
  tmp = 19;
  ecx = #x86_ROR32(ecx, tmp); 
  tmp = 10;
  edx = #x86_SHR32(edx, tmp);
  tmp = 17;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);

  x = SCHED(i, x);
  [rsp + x] = ebx;

  sebx = ebx;
  tl = roundtail(tl, k, sebx);

  return tl;
}

fn rotateTL(reg u64[8] tl) -> reg u64[8] 
{
  reg u64 t;

  t = tl[7];

  tl[7] = tl[6];
  tl[6] = tl[5];
  tl[5] = tl[4];
  tl[4] = tl[3];
  tl[3] = tl[2];
  tl[2] = tl[1];
  tl[1] = tl[0];
  tl[0] = t;
  
  return tl;
}

export fn crypto_hashblocks(reg u64 sb, reg u64 in, reg u64 w, reg u64 inlen)
{
  inline int i;
  inline int k;
  inline int tmp;
  stack u64[8] state;
  reg u64[8] tl;

  /* Save registers, allocate scratch space */ 
  state[0] = [sb + 0*8];
  state[1] = [sb + 1*8];
  state[2] = [sb + 2*8];
  state[3] = [sb + 3*8];
  state[4] = [sb + 4*8];
  state[5] = [sb + 5*8];  
  state[6] = [sb + 6*8];
  state[7] = [sb + 7*8]; 

  /* Load state */
  tl[0] = state[0];
  tl[1] = state[1]; 
  tl[2] = state[2]; 
  tl[3] = state[3];
  tl[4] = state[4];
  tl[5] = state[5];
  tl[6] = state[6]; 
  tl[7] = state[7];
  
  /*tmp = 64;
  while (inlen >= tmp){*/
  k = 0x428A2F98; i = 0;  tl = ROUNDa(i, tl, in, k, w); tl = rotateTL(tl);
  k = 0x71374491; i = 1;  tl = ROUNDa(i, tl, in, k, w); tl = rotateTL(tl);
  k = 0x4A3F0431; i = 2;  tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x164A245B; i = 3;  tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x3956C25B; i = 4;  tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x59F111F1; i = 5;  tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x6DC07D5C; i = 6;  tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x54E3A12B; i = 7;  tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x27F85568; i = 8;  tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x12835B01; i = 9;  tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x243185BE; i = 10; tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x550C7DC3; i = 11; tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x72BE5D74; i = 12; tl = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
  k = 0x7F214E02; i = 13; tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x6423F959; i = 14; tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
  k = 0x3E640E8C; i = 15; tl = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl); //648-683

  k = 0x1B64963F; i = 0; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x1041B87A; i = 1; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x0FC19DC6; i = 2; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x240CA1CC; i = 3; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x2DE92C6F; i = 4; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x4A7484AA; i = 5; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x5CB0A9DC; i = 6; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x76F988DA; i = 7; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x67C1AEAE; i = 8; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x57CE3993; i = 9; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x4FFCD838; i = 10; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x40A68039; i = 11; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x391FF40D; i = 12; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x2A586EB9; i = 13; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x06CA6351; i = 14; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x14292967; i = 15; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x27B70A85; i = 0; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x2E1B2138; i = 1; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x4D2C6DFC; i = 2; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x53380D13; i = 3; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x650A7354; i = 4; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x766A0ABB; i = 5; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x7E3D36D2; i = 6; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x6D8DD37B; i = 7; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x5D40175F; i = 8; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x57E599B5; i = 9; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x3DB47490; i = 10; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x3893AE5D; i = 11; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x2E6D17E7; i = 12; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x2966F9DC; i = 13; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x0BF1CA7B; i = 14; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x106AA070; i = 15; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x19A4C116; i = 0; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl); 
  k = 0x1E376C08; i = 1; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x2748774C; i = 2; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x34B0BCB5; i = 3; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x391C0CB3; i = 4; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x4ED8AA4A; i = 5; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x5B9CCA4F; i = 6; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x682E6FF3; i = 7; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x748F82EE; i = 8; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x78A5636F; i = 9; tl = ROUNDb(i,  tl, k, w); tl = rotateTL(tl);
  k = 0x7B3787EC; i = 10; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x7338FDF8; i = 11; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x6F410006; i = 12; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x5BAF9315; i = 13; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x41065C09; i = 14; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
  k = 0x398E870E; i = 15; tl = ROUNDbX(i, tl, k, w); tl = rotateTL(tl);
/*
    in += tmp;
    inlen -= tmp;
  }*/

  /* Add to state */
  tl[0] += state[0]; state[0] = tl[0];
  tl[1] += state[1]; state[1] = tl[1];
  tl[2] += state[2]; state[2] = tl[2];
  tl[3] += state[3]; state[3] = tl[3];
  tl[4] += state[4]; state[4] = tl[4];
  tl[5] += state[5]; state[5] = tl[5];
  tl[6] += state[6]; state[6] = tl[6];
  tl[7] += state[7]; state[7] = tl[7];
  
  /* Restore registers */
  [sb + 0*8] = state[0];
  [sb + 1*8] = state[1];
  [sb + 2*8] = state[2];
  [sb + 3*8] = state[3];
  [sb + 4*8] = state[4];
  [sb + 5*8] = state[5];
  [sb + 6*8] = state[6];
  [sb + 7*8] = state[7];

}
