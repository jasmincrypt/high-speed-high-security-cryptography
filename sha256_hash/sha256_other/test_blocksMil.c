#include "stdio.h"

typedef unsigned long int u64;

extern void crypto_hashblocks(u64[], const unsigned char*, u64[]); 
/*extern void test_unpack(u64[], u64[]);
extern void test_mul(u64[], u64[], u64[]);
extern void scalarmult(u64[], u64[], u64[]);
extern int test_ladderstep(u64[], u64[], u64[],u64[], u64[], u64[]);
extern void test_mladder(u64[], u64[], u64[]);*/
int main() {
        
        /*u64 b[4] = { 9, 0, 0, 0};
        u64 e[4] = {0x7da518730a6d0777,0x4566b25172c1163c,
        	        0x2a99c0eb872f4cdf,0x2a2cb91da5fb77b1};
        u64 r[4];
        scalarmult(r,e,b);
        printf("0x%016lx%016lx%016lx%016lx\n",r[3],r[2],r[1],r[0]);
        //----------------------------------
*/
	//u64 h[8]; 
	u64 h[8];
	h[0] = 0x6a09e667; 
	h[1] = 0xbb67ae85; 
	h[2] = 0x3c6ef372; 
	h[3] = 0xa54ff53a; 
	h[4] = 0x510e527f; 
	h[5] = 0x9b05688c; 
	h[6] = 0x1f83d9ab; 
	h[7] = 0x5be0cd19; 

	static const char tmpin[64] = {
	  0x6a,0x09,0xe6,0x67,
	  0xbb,0x67,0xae,0x85,
	  0x3c,0x6e,0xf3,0x72,
	  0xa5,0x4f,0xf5,0x3a,
	  0x51,0x0e,0x52,0x7f,
	  0x9b,0x05,0x68,0x8c,
	  0x1f,0x83,0xd9,0xab,
	  0x5b,0xe0,0xcd,0x19,
	  0x6a,0x09,0xe6,0x67,
	  0xbb,0x67,0xae,0x85,
	  0x3c,0x6e,0xf3,0x72,
	  0xa5,0x4f,0xf5,0x3a,
	  0x51,0x0e,0x52,0x7f,
	  0x9b,0x05,0x68,0x8c,
	  0x1f,0x83,0xd9,0xab,
	  0x5b,0xe0,0xcd,0x19,
	} ;

	int i;
	unsigned char in[64];
	for (i = 0;i < 64;++i) in[i] = tmpin[i];

	/*int i; 
	u64 in[16]; 
	for (i = 0;i < 16;++i) {
		in[i] = i * i * i + i * 0x9e3779b9; 
	}*/
	
	//uint64_t len; 			
	//len = 64;
        u64 rsp[16];

	crypto_hashblocks(h, in, rsp);
	
 	/*printf("0x000000001f60506d - 0x000000003373e350 - 0x000000004bf88213 - 0x00000000a76cc98b - 0x00000000b263dc3a - 0x00000000dd1932ca - 0x000000000cf3187b - 0x000000000ac838ae");*/
	printf("\n");
	printf("--------------------------------------");
	printf("\n");

	for (i = 0;i < 8;++i) {
		printf("%x - ", h[i]);
	}
	printf("\n");



      /*
        u64 one[4] = { 1, 0, 0,0};
        //u64 e[4] = {0x7da518730a6d0777,0x4566b25172c1163c,
        //            0x2a99c0eb872f4cdf,0x2a2cb91da5fb77b1};
        u64 e[4] = { 9, 0, 0, 0};
        u64 zero[4] = { 0, 0, 0, 0};
        u64 r[4*4];
        test_ladderstep(e,one,zero,e,one,r);
        
        printf("0x%016lx%016lx%016lx%016lx\n",r[3],r[2],r[1],r[0]);
        printf("0x%016lx%016lx%016lx%016lx\n",r[7],r[6],r[5],r[4]);
        printf("0x%016lx%016lx%016lx%016lx\n",r[11],r[10],r[9],r[8]);
        printf("0x%016lx%016lx%016lx%016lx\n",r[15],r[14],r[13],r[12]);*/

        /*printf("0x%016lx %016lx %016lx %016lx %016lx\n",r[4],r[3],r[2],r[1],r[0]);
        printf("0x%016lx %016lx %016lx %016lx %016lx\n",r[9],r[8],r[7],r[6],r[5]);
        printf("0x%016lx %016lx %016lx %016lx %016lx\n",r[14],r[13],r[12],r[11],r[10]);
        printf("0x%016lx %016lx %016lx %016lx %016lx\n",r[19],r[18],r[17],r[16],r[15]);*/

/*
        u64 b[4] = { 9, 0, 0, 0};
        u64 e[4] = {0x7da518730a6d0776,0x4566b25172c1163c,
        	          0x2a99c0eb872f4cdf,0x2a2cb91da5fb77b1};
        //u64 e[4] = { 0, 1, 0, 0};
        u64 r[2*4];
        test_mladder(r,e,b);
        printf("0x%016lx%016lx%016lx%016lx\n",r[3],r[2],r[1],r[0]);
        printf("0x%016lx%016lx%016lx%016lx\n",r[7],r[6],r[5],r[4]);*/
/*
        u64 a[4] = {0xffffffffffffffeb, 0xffffffffffffffff, 0xffffffffffffffff, 0x7fffffffffffffff};
        u64 b[4] = {9,0, 0,0};
        u64 r[4];
        test_mul(r,b,a);
        printf("0x%016lx%016lx%016lx%016lx\n",r[3],r[2],r[1],r[0]);*/

/*
        u64 e[4] = {0x7da518730a6d0777,0x4566b25172c1163c,
                          0x2a99c0eb872f4cdf,0x2a2cb91da5fb77b1};
        u64 r[5];
        test_unpack(r,e);
        printf("0x%lx %lx %lx %lx\n",e[3],e[2],e[1],e[0]);
        printf("0x%lx %lx %lx %lx %lx\n",r[4],r[3],r[2],r[1],r[0]);
*/
} 

// 2a2cb91da5fb77b12a99c0eb872f4cdf4566b25172c1163c7da518730a6d0777
