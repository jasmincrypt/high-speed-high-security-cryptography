fn roundtail(reg u64[8] tl, inline int k, reg u64 ebx) -> reg u64[8]
{
  reg u64 ecx;
  reg u64 edx;
  reg u64 eax;
  inline int tmp; 

  /* Part 0 */
  ecx = tl[4];
  edx = tl[4];
  eax = tl[4];
  
  tmp = 11;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 25;
  edx = #x86_ROR32(edx, tmp);
  tmp = 6;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  tl[7] = #x86_ADD32(tl[7], ebx);
  ecx = tl[6];
  ecx ^= tl[5];
  ecx &= tl[4];
  ecx ^= tl[6];

  eax = k + ecx + eax;
  tl[7] = #x86_ADD32(tl[7], eax);

  /* Part 1 */
  tl[3] = #x86_ADD32(tl[3], tl[7]);

  /* Part 2 */
  ecx = tl[0];
  edx = tl[0];
  eax = tl[0];

  tmp = 13;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 22;
  edx = #x86_ROR32(edx, tmp);
  tmp = 2;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  ecx = tl[2];
  tl[7] = #x86_ADD32(tl[7], eax);

  eax = tl[2];
  eax |= tl[1];
  ecx &= tl[1];
  eax &= tl[0];
  eax |= ecx;
  tl[7] = #x86_ADD32(tl[7], eax);

  return tl;
}

fn roundtailX(reg u64[8] tl, inline int k, reg u64 ebx) -> reg u64[8]
{
  reg u64 ecx;
  reg u64 edx;
  reg u64 eax;
  inline int tmp; 

  /* Part 0 */
  ecx = tl[4];
  edx = tl[4];
  eax = tl[4];
  
  tmp = 11;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 25;
  edx = #x86_ROR32(edx, tmp);
  tmp = 6;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  tl[7] = #x86_ADD32(tl[7], ebx);
  ecx = tl[6];
  ecx ^= tl[5];
  ecx &= tl[4];
  ecx ^= tl[6];

  eax = -k + ecx + eax;
  tl[7] = #x86_ADD32(tl[7], eax);

  /* Part 1 */
  tl[3] = #x86_ADD32(tl[3], tl[7]);

  /* Part 2 */
  ecx = tl[0];
  edx = tl[0];
  eax = tl[0];

  tmp = 13;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 22;
  edx = #x86_ROR32(edx, tmp);
  tmp = 2;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx;

  ecx = tl[2];
  tl[7] = #x86_ADD32(tl[7], eax);

  eax = tl[2];
  eax |= tl[1];
  ecx &= tl[1];
  eax &= tl[0];
  eax |= ecx;
  tl[7] = #x86_ADD32(tl[7], eax);

  return tl;
}


fn ROUNDa(inline int i, reg u64[8] tl, reg u64 rsi, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 ebx;
  reg u64 x;
  
  ebx = #x86_MOV32([rsi + i*4]);
  ebx = #x86_BSWAP32(ebx);

  rsp[i] = ebx;

  tl = roundtail(tl, k, ebx);
  return tl, rsp;
}

fn ROUNDaX(inline int i, reg u64[8] tl, reg u64 rsi, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 ebx;
  reg u64 x;
  
  ebx = #x86_MOV32([rsi + i*4]);
  ebx = #x86_BSWAP32(ebx);

  rsp[i] = ebx;

  tl = roundtailX(tl, k, ebx);
  return tl, rsp;
}

fn ROUNDaR(inline int i, inline int round, reg u64[8] tl, stack u64[32] rsi, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 ebx;
  reg u64 x;
  
  ebx = rsi[round+i];
  ebx = #x86_BSWAP32(ebx);

  rsp[i] = ebx;

  tl = roundtail(tl, k, ebx);
  return tl, rsp;
}

fn ROUNDaXR(inline int i, inline int round, reg u64[8] tl, stack u64[32] rsi, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 ebx;
  reg u64 x;
  
  ebx = rsi[round+i];
  ebx = #x86_BSWAP32(ebx);

  rsp[i] = ebx;

  tl = roundtailX(tl, k, ebx);
  return tl, rsp;
}

fn ROUNDb(inline int i, inline int i1, inline int i2, inline int i3, reg u64[8] tl, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 eax;
  reg u64 ebx;
  reg u64 ecx;
  reg u64 edx;
  inline int tmp;

  eax = rsp[i1];
  ebx = rsp[i];
  ecx = rsp[i2];
  ebx = #x86_ADD32(ebx, ecx);

  ecx = eax;
  edx = eax;

  tmp = 18;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 3;
  edx = #x86_SHR32(edx, tmp);
  tmp = 7;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);
  
  eax = rsp[i3];

  ecx = eax;
  edx = eax;
  
  tmp = 19;
  ecx = #x86_ROR32(ecx, tmp); 
  tmp = 10;
  edx = #x86_SHR32(edx, tmp);
  tmp = 17;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);

  rsp[i] = ebx;

  tl = roundtail(tl, k, ebx);

  return tl, rsp;
}

fn ROUNDbX(inline int i, inline int i1, inline int i2, inline int i3, reg u64[8] tl, inline int k, stack u64[16] rsp) -> reg u64[8], stack u64[16]
{
  reg u64 eax;
  reg u64 ebx;
  reg u64 ecx;
  reg u64 edx;
  inline int tmp;

  eax = rsp[i1];
  ebx = rsp[i];
  ecx = rsp[i2];
  ebx = #x86_ADD32(ebx, ecx);

  ecx = eax;
  edx = eax;

  tmp = 18;
  ecx = #x86_ROR32(ecx, tmp);
  tmp = 3;
  edx = #x86_SHR32(edx, tmp);
  tmp = 7;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);
  
  eax = rsp[i3];

  ecx = eax;
  edx = eax;
  
  tmp = 19;
  ecx = #x86_ROR32(ecx, tmp); 
  tmp = 10;
  edx = #x86_SHR32(edx, tmp);
  tmp = 17;
  eax = #x86_ROR32(eax, tmp);

  ecx ^= edx;
  eax ^= ecx; 

  ebx = #x86_ADD32(ebx, eax);

  rsp[i] = ebx;

  tl = roundtailX(tl, k, ebx);

  return tl, rsp;
}

fn rotateTL(reg u64[8] tl) -> reg u64[8] 
{
  reg u64 t;

  t = tl[7];

  tl[7] = tl[6];
  tl[6] = tl[5];
  tl[5] = tl[4];
  tl[4] = tl[3];
  tl[3] = tl[2];
  tl[2] = tl[1];
  tl[1] = tl[0];
  tl[0] = t;
  
  return tl;
}

fn blocks(stack u64[8] state, reg u64 in, reg u64 inlen) -> stack u64[8], reg u64, reg u64
{
  inline int i;
  inline int i1;
  inline int i2;
  inline int i3;
  inline int k;
  inline int tmp;
  stack u64[16] w;
  reg u64[8] tl;

  /* Load state */
  tl[0] = state[0];
  tl[1] = state[1]; 
  tl[2] = state[2]; 
  tl[3] = state[3];
  tl[4] = state[4];
  tl[5] = state[5];
  tl[6] = state[6]; 
  tl[7] = state[7];
  
  tmp = 64;
  while (inlen >= tmp){

    k = 0x428A2F98; i = 0;  tl, w = ROUNDa(i, tl, in, k, w); tl = rotateTL(tl);
    k = 0x71374491; i = 1;  tl, w = ROUNDa(i, tl, in, k, w); tl = rotateTL(tl);
    k = 0x4A3F0431; i = 2;  tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x164A245B; i = 3;  tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x3956C25B; i = 4;  tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x59F111F1; i = 5;  tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x6DC07D5C; i = 6;  tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x54E3A12B; i = 7;  tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x27F85568; i = 8;  tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x12835B01; i = 9;  tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x243185BE; i = 10; tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x550C7DC3; i = 11; tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x72BE5D74; i = 12; tl, w = ROUNDa(i,  tl, in, k, w);tl = rotateTL(tl);
    k = 0x7F214E02; i = 13; tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x6423F959; i = 14; tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl);
    k = 0x3E640E8C; i = 15; tl, w = ROUNDaX(i, tl, in, k, w);tl = rotateTL(tl); //648-683
    
    k = 0x1B64963F; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x1041B87A; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x0FC19DC6; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x240CA1CC; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2DE92C6F; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4A7484AA; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5CB0A9DC; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x76F988DA; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x67C1AEAE; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x57CE3993; i = 9; i1 = 10; i2 =2 ; i3 =7 ;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4FFCD838; i = 10;i1 = 11; i2 =3 ; i3 =8 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x40A68039; i = 11;i1 = 12; i2 =4 ; i3 =9 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x391FF40D; i = 12;i1 = 13; i2 =5 ; i3 =10 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2A586EB9; i = 13;i1 = 14; i2 =6 ; i3 = 11;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x06CA6351; i = 14;i1 = 15; i2 =7 ; i3 = 12;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x14292967; i = 15;i1 = 0; i2 = 8; i3 = 13;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x27B70A85; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2E1B2138; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4D2C6DFC; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x53380D13; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x650A7354; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x766A0ABB; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7E3D36D2; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x6D8DD37B; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5D40175F; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x57E599B5; i = 9; i1 = 10; i2 =2 ; i3 =7 ;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x3DB47490; i = 10;i1 = 11; i2 =3 ; i3 =8 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x3893AE5D; i = 11;i1 = 12; i2 =4 ; i3 =9 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2E6D17E7; i = 12;i1 = 13; i2 =5 ; i3 = 10;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2966F9DC; i = 13;i1 = 14; i2 =6 ; i3 = 11;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x0BF1CA7B; i = 14;i1 = 15; i2 =7 ; i3 = 12;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x106AA070; i = 15;i1 = 0; i2 = 8; i3 = 13;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x19A4C116; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl); 
    k = 0x1E376C08; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2748774C; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x34B0BCB5; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x391C0CB3; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4ED8AA4A; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5B9CCA4F; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x682E6FF3; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x748F82EE; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x78A5636F; i = 9; i1 = 10; i2 = 2; i3 =7 ;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7B3787EC; i = 10; i1 = 11; i2 =3 ; i3 = 8;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7338FDF8; i = 11; i1 = 12; i2 =4 ; i3 = 9;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x6F410006; i = 12; i1 = 13; i2 =5 ; i3 = 10;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5BAF9315; i = 13; i1 = 14; i2 =6 ; i3 = 11;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x41065C09; i = 14; i1 = 15; i2 =7 ; i3 = 12;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x398E870E; i = 15; i1 = 0; i2 = 8; i3 = 13;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);

    /* Add to state */
    tl[0] += state[0]; state[0] = tl[0];
    tl[1] += state[1]; state[1] = tl[1];
    tl[2] += state[2]; state[2] = tl[2];
    tl[3] += state[3]; state[3] = tl[3];
    tl[4] += state[4]; state[4] = tl[4];
    tl[5] += state[5]; state[5] = tl[5];
    tl[6] += state[6]; state[6] = tl[6];
    tl[7] += state[7]; state[7] = tl[7];

    in += tmp;
    inlen -= tmp;
  }
  
  return state, in, inlen;
}

fn blocks_padded(stack u64[8] state, stack u64[32] in, reg u64 inlen) -> stack u64[8]
{
  inline int i;
  inline int i1;
  inline int i2;
  inline int i3;
  inline int k;
  inline int j;
  inline int tmp;
  inline int round;
  stack u64[16] w;
  reg u64[8] tl;

  /* Load state */
  tl[0] = state[0];
  tl[1] = state[1]; 
  tl[2] = state[2]; 
  tl[3] = state[3];
  tl[4] = state[4];
  tl[5] = state[5];
  tl[6] = state[6]; 
  tl[7] = state[7];
  
  tmp = 64;
  round = 0;
  
  for j = 0 to 8 {// 128/16
  round = j*16;
  if (inlen >= tmp){

    k = 0x428A2F98; i = 0;  tl, w = ROUNDaR(i,  round, tl, in, k, w); tl = rotateTL(tl);
    k = 0x71374491; i = 1;  tl, w = ROUNDaR(i,  round, tl, in, k, w); tl = rotateTL(tl);
    k = 0x4A3F0431; i = 2;  tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x164A245B; i = 3;  tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x3956C25B; i = 4;  tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x59F111F1; i = 5;  tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x6DC07D5C; i = 6;  tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x54E3A12B; i = 7;  tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x27F85568; i = 8;  tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x12835B01; i = 9;  tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x243185BE; i = 10; tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x550C7DC3; i = 11; tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x72BE5D74; i = 12; tl, w = ROUNDaR(i,  round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x7F214E02; i = 13; tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x6423F959; i = 14; tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl);
    k = 0x3E640E8C; i = 15; tl, w = ROUNDaXR(i, round, tl, in, k, w);tl = rotateTL(tl); //648-683
    
    k = 0x1B64963F; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x1041B87A; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x0FC19DC6; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x240CA1CC; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2DE92C6F; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4A7484AA; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5CB0A9DC; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x76F988DA; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x67C1AEAE; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x57CE3993; i = 9; i1 = 10; i2 =2 ; i3 =7 ;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4FFCD838; i = 10;i1 = 11; i2 =3 ; i3 =8 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x40A68039; i = 11;i1 = 12; i2 =4 ; i3 =9 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x391FF40D; i = 12;i1 = 13; i2 =5 ; i3 =10 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2A586EB9; i = 13;i1 = 14; i2 =6 ; i3 = 11;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x06CA6351; i = 14;i1 = 15; i2 =7 ; i3 = 12;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x14292967; i = 15;i1 = 0; i2 = 8; i3 = 13;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x27B70A85; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2E1B2138; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4D2C6DFC; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x53380D13; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x650A7354; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x766A0ABB; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7E3D36D2; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x6D8DD37B; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5D40175F; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x57E599B5; i = 9; i1 = 10; i2 =2 ; i3 =7 ;
    tl, w = ROUNDbX(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x3DB47490; i = 10;i1 = 11; i2 =3 ; i3 =8 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x3893AE5D; i = 11;i1 = 12; i2 =4 ; i3 =9 ;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2E6D17E7; i = 12;i1 = 13; i2 =5 ; i3 = 10;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2966F9DC; i = 13;i1 = 14; i2 =6 ; i3 = 11;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x0BF1CA7B; i = 14;i1 = 15; i2 =7 ; i3 = 12;
     tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x106AA070; i = 15;i1 = 0; i2 = 8; i3 = 13;
     tl, w = ROUNDb(i, i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x19A4C116; i = 0; i1 = 1; i2 = 9; i3 = 14;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl); 
    k = 0x1E376C08; i = 1; i1 = 2; i2 = 10; i3 = 15;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x2748774C; i = 2; i1 = 3; i2 = 11; i3 = 0;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x34B0BCB5; i = 3; i1 = 4; i2 = 12; i3 = 1;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x391C0CB3; i = 4; i1 = 5; i2 = 13; i3 = 2;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x4ED8AA4A; i = 5; i1 = 6; i2 = 14; i3 = 3;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5B9CCA4F; i = 6; i1 = 7; i2 = 15; i3 = 4;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x682E6FF3; i = 7; i1 = 8; i2 = 0; i3 = 5;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x748F82EE; i = 8; i1 = 9; i2 = 1; i3 = 6;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x78A5636F; i = 9; i1 = 10; i2 = 2; i3 =7 ;
    tl, w = ROUNDb(i,  i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7B3787EC; i = 10; i1 = 11; i2 =3 ; i3 = 8;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x7338FDF8; i = 11; i1 = 12; i2 =4 ; i3 = 9;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x6F410006; i = 12; i1 = 13; i2 =5 ; i3 = 10;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x5BAF9315; i = 13; i1 = 14; i2 =6 ; i3 = 11;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x41065C09; i = 14; i1 = 15; i2 =7 ; i3 = 12;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);
    k = 0x398E870E; i = 15; i1 = 0; i2 = 8; i3 = 13;
    tl, w = ROUNDbX(i,i1, i2, i3, tl, k, w); tl = rotateTL(tl);

    /* Add to state */
    tl[0] += state[0]; state[0] = tl[0];
    tl[1] += state[1]; state[1] = tl[1];
    tl[2] += state[2]; state[2] = tl[2];
    tl[3] += state[3]; state[3] = tl[3];
    tl[4] += state[4]; state[4] = tl[4];
    tl[5] += state[5]; state[5] = tl[5];
    tl[6] += state[6]; state[6] = tl[6];
    tl[7] += state[7]; state[7] = tl[7];

    state[0] = tl[0];
    state[1] = tl[1];
    state[2] = tl[2];
    state[3] = tl[3];
    state[4] = tl[4];
    state[5] = tl[5];
    state[6] = tl[6];
    state[7] = tl[7];

    inlen -= 64;
  }/*else{
    break;
  }*/
    
  }//for

  return state;
}
/*-----------------------------------------------------*/
/*-------------------------HASH------------------------*/
/*-----------------------------------------------------*/
fn getIV(stack u64[8] iv) -> stack u64[8]
{
  reg u64 x;

  iv[0] = 0x6a09e667;
  x = 0xbb67ae85;
  iv[1] = x;
  iv[2] = 0x3c6ef372;
  x = 0xa54ff53a;
  iv[3] = x;
  iv[4] = 0x510e527f;
  x = 0x9b05688c;
  iv[5] = x;
  iv[6] = 0x1f83d9ab;
  iv[7] = 0x5be0cd19;

  return iv;
}

export fn crypto_hash(reg u64 out, reg u64 in, reg u64 inlen)
-> reg u64
{
  stack u64[8] state;
  stack u64[32] padded;
  stack u64 bits;
  reg u64 ret;
  inline int i;
  inline int inlenint;
  reg u64 tmpInlen;
  //256-64//128-32//32-8
  inlenint = 8;//inlen/4

  
  bits = inlen;

  state = getIV(state);
  //For a test
  /*for i = 0 to 32 { ret = #x86_MOV32([in + i*4]); 
	padded[i] = ret;}
  state = blocks_padded(state, padded, inlen);*/
  state, in, inlen = blocks(state, in, inlen);//d3ce
  /*in += inlen;
  tmpInlen = bits;//variable inlen already set
  tmpInlen &= 63;
  in -= tmpInlen;*/

  ret = bits;
  ret = #x86_SHL32(ret, 3);
  bits = ret;

  

  /*tmpInlen += 3;
  tmpInlen >>= 2;*/
/*
  while (tmpInlen > 0){
    ret = #x86_MOV32([in + tmpInlen*4]); 
    padded[tmpInlen] = ret;
    tmpInlen -= 1;
  }
  */
  /*for i = 0 to inlenint {
    ret = #x86_MOV32([in + i*4]);
    padded[i] = ret;
  }*/


  ret = 0x80;
  //ret <<= 24;
  padded[inlenint] = ret;

  if (inlen < 56) {
    for i = inlenint+1 to 14 { [in + i*4] = 0; }
    ret = bits;
    ret = #x86_SHR32(ret, 48);
    padded[14] = ret;
    ret = bits;
    ret = #x86_SHL32(ret, 8);
    padded[15] = ret;

    ret = 64;
    state = blocks(state,padded,ret);
  } else {
    for i = inlenint+1 to 120 { [in + i*4] = 0; }
    ret = bits;
    ret = #x86_SHR32(ret, 48);
    padded[30] = ret;
    ret = bits;
    ret = #x86_SHL32(ret, 8);
    padded[31] = ret;

    ret = 128;
    state = blocks(state,padded,ret);
  }

  /* Restore registers */
  [out + 0*8] = state[0];
  [out + 1*8] = state[1];
  [out + 2*8] = state[2];
  [out + 3*8] = state[3];
  [out + 4*8] = state[4];
  [out + 5*8] = state[5];
  [out + 6*8] = state[6];
  [out + 7*8] = state[7];

  ret = 0;
  return ret;
}

/*-----------------------------------OTHER----------
export fn crypto_hash(reg u64 out, reg u64 in, reg u64 inlen)
-> reg u64
{
  stack u64[8] state;
  stack u64[128] padded;
  reg u64 bits;
  inline int i;
  reg u64 ret;
  inline int tmpinlen;
  tmpinlen = 0;
  
  state = getIV(state);
  state, in, inlen = blocks(state, in, inlen);

  bits = inlen;
  bits <<= 3;

  for i = 0 to tmpinlen { padded[i] = #x86_MOV32([in + i*4]); }
  padded[tmpinlen] = 0x80;
  
  if (inlen < 56) {
    for i = tmpinlen + 1 to 56 { padded[i] = 0; }
    padded[56] = bits >> 56;
    padded[57] = bits >> 48;
    padded[58] = bits >> 40;
    padded[59] = bits >> 32;
    padded[60] = bits >> 24;
    padded[61] = bits >> 16;
    padded[62] = bits >> 8;
    padded[63] = bits;
    blocks(state,padded,64);
  } else {
    for i = tmpinlen + 1 to 120 { padded[i] = 0; }
    padded[120] = bits >> 56;
    padded[121] = bits >> 48;
    padded[122] = bits >> 40;
    padded[123] = bits >> 32;
    padded[124] = bits >> 24;
    padded[125] = bits >> 16;
    padded[126] = bits >> 8;
    padded[127] = bits;
    blocks(state,padded,128);
  }*/

  /* Restore registers */
  /*[out + 0*8] = state[0];
  [out + 1*8] = state[1];
  [out + 2*8] = state[2];
  [out + 3*8] = state[3];
  [out + 4*8] = state[4];
  [out + 5*8] = state[5];
  [out + 6*8] = state[6];
  [out + 7*8] = state[7];

  ret = 0;
  return ret;
}*/

//export fn crypto_hash(reg u64 out, reg u64 in, reg u64 inlen)
//{
  /*inline int i;
  reg u64 bits;
  reg u64 tmp;*/
  //stack u64[8] state;
  //inline int j;
  //stack u64 inlenS;
  /*inline int tinlen;

  tinlen=128;*/
  /*Set IV*/
  /*iv = getIV(iv);
  for j = 0 to 8{
    [sb + j*8] = iv[j];
  }*/

  //inlenS = inlen;
  //state = getIV(state);
  //state = blocks(state,in,inlen);
  //inlen = inlenS;
  //in += inlen;
  //inlen &= 63;
  //in -= inlen;

  //bits = inlen;
  //bits = #x86_SHL32(bits, 3);

  //i = 0;
  //while (i < inlen){
  //for i = 0 to tinlen {
    //[padded + i*4] = #x86_MOV32([in + i*4]);
    //i+=1;
  //}
  //[padded + tinlen*4] = #x86_MOV32(0x80);//?

  /*if (inlen < 56) {
    //i = inlen+1;
    //while (i < 56) {
    for i = tinlen+1 to 56 {
      //[padded + i*4] = #x86_MOV32(0);
      //i+=1;
    }
*/
    /*tmp = #x86_SHR32(bits, 56);
    [padded + 56*4] = tmp;
    [padded + 57*4] = #x86_SHR32(bits, 48);
    [padded + 58*4] = #x86_SHR32(bits, 40);
    [padded + 59*4] = #x86_SHR32(bits, 32);
    [padded + 60*4] = #x86_SHR32(bits, 24);
    [padded + 61*4] = #x86_SHR32(bits, 16);
    [padded + 62*4] = #x86_SHR32(bits, 8);
    [padded + 63*4] = #x86_MOV32(bits);*/

    //i = 64;
    //blocks(sb,padded,i);
/*  } else {
    //i = inlen + 1;
    //while (i < 120) {
    for i = tinlen+1 to 120{
      //[padded + i*4] = #x86_MOV32(0);
      //i+=1;
    } 
*/
    /*[padded + 120*4] = #x86_SHR32(bits, 56);
    [padded + 121*4] = #x86_SHR32(bits, 48);
    [padded + 122*4] = #x86_SHR32(bits, 40);
    [padded + 123*4] = #x86_SHR32(bits, 32);
    [padded + 124*4] = #x86_SHR32(bits, 24);
    [padded + 125*4] = #x86_SHR32(bits, 16);
    [padded + 126*4] = #x86_SHR32(bits, 8);
    [padded + 127*4] = #x86_MOV32(bits);*/

    //i = 128;
    //blocks(sb,padded,i);
  //}

  /* Restore registers */
  /*[out + 0*8] = state[0];
  [out + 1*8] = state[1];
  [out + 2*8] = state[2];
  [out + 3*8] = state[3];
  [out + 4*8] = state[4];
  [out + 5*8] = state[5];
  [out + 6*8] = state[6];
  [out + 7*8] = state[7];
}*/


















