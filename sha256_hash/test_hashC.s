	.file	"test_hashC.c"
	.text
	.type	load_bigendian, @function
load_bigendian:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$3, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movq	-8(%rbp), %rdx
	addq	$2, %rdx
	movzbl	(%rdx), %edx
	movzbl	%dl, %edx
	sall	$8, %edx
	orl	%eax, %edx
	movq	-8(%rbp), %rax
	addq	$1, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$16, %eax
	orl	%eax, %edx
	movq	-8(%rbp), %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	sall	$24, %eax
	orl	%edx, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	load_bigendian, .-load_bigendian
	.section	.rodata
.LC0:
	.string	"%x - "
	.text
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$320, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -312(%rbp)
	jmp	.L4
.L5:
	movl	-312(%rbp), %eax
	cltq
	movzbl	tmpin.2294(%rax), %eax
	movl	%eax, %edx
	movl	-312(%rbp), %eax
	cltq
	movb	%dl, -272(%rbp,%rax)
	addl	$1, -312(%rbp)
.L4:
	cmpl	$255, -312(%rbp)
	jle	.L5
	leaq	-272(%rbp), %rcx
	leaq	-304(%rbp), %rax
	movl	$135, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	crypto_hash
	movl	%eax, -308(%rbp)
	movl	$0, -312(%rbp)
	jmp	.L6
.L7:
	movl	-312(%rbp), %eax
	sall	$2, %eax
	cltq
	leaq	-304(%rbp), %rdx
	addq	%rdx, %rax
	movq	%rax, %rdi
	call	load_bigendian
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	addl	$1, -312(%rbp)
.L6:
	cmpl	$7, -312(%rbp)
	jle	.L7
	movl	$10, %edi
	call	putchar
	movl	$0, %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L9
	call	__stack_chk_fail
.L9:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.section	.rodata
	.align 32
	.type	tmpin.2294, @object
	.size	tmpin.2294, 256
tmpin.2294:
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.byte	106
	.byte	9
	.byte	-26
	.byte	103
	.byte	-69
	.byte	103
	.byte	-82
	.byte	-123
	.byte	60
	.byte	110
	.byte	-13
	.byte	114
	.byte	-91
	.byte	79
	.byte	-11
	.byte	58
	.byte	81
	.byte	14
	.byte	82
	.byte	127
	.byte	-101
	.byte	5
	.byte	104
	.byte	-116
	.byte	31
	.byte	-125
	.byte	-39
	.byte	-85
	.byte	91
	.byte	-32
	.byte	-51
	.byte	25
	.zero	64
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
